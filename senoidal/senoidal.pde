/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
* Autor: Luis Alberto Oropeza Vilchis <luis.oropeza.129@gmail.com>
*/

/*
* Animación que muestra la generación de valores de tres señales
* senoidales.
*/


import java.util.*; // Para utilizar la clase Vector

SinusoidalShape sin1, sin2, sin3;

void setup() {
  size(512, 512);
  sin1 = new SinusoidalShape(16*PI, 5, 10, 0.05);
  sin2 = new SinusoidalShape(12*PI, 6, 15, 0.05);
  sin3 = new SinusoidalShape(8*PI, 7, 20, 0.05);
}

void draw() {
  background(255);
  strokeWeight(4);
  stroke(150, 0, 0); // Color rojo
  sin1.draw();
  stroke(0, 150, 0); // Color verde
  sin2.draw();
  stroke(0, 0, 150); // Color azul
  sin3.draw();
}

public class SinusoidalShape {

  private float period;
  private float amplitud;
  private float precision;
  private float scale;
  private Vector<Point> points;

  SinusoidalShape(float period, float amplitud, float scale, float precision) {
    this.period = period;
    this.amplitud = amplitud;
    this.scale = scale;
    this.precision = precision;
    this.points = new Vector(100);
    calculate();
  }


  private void calculate() {
    for (float i = 0; i <= period; i += precision) {
      points.add(new Point(i * scale, amplitud*sin(i)*scale));
    }
  }

  public void draw() {
    for (int i=0; i < points.size(); i+=1) {
      point(points.elementAt(i).getX(), points.elementAt(i).getY() + height/2);
      points.elementAt(i).translateX(-precision*scale);
    }
    Point p = points.remove(0);
    p.translateX(period*scale);
    points.add(p);
  }
}

public class Point {
  float x, y;

  Point() {
    this.x = 0.0;
    this.y = 0.0;
  }

  Point(float x, float y) {
    this.x = x;
    this.y = y;
  }

  public void setX(float x) {
    this.x = x;
  }

  public float getX() {
    return x;
  }

  public void setY(float y) {
    this.x = y;
  }

  public float getY() {
    return y;
  }

  public void translateX(float offset) {
    this.x += offset;
  }
}
