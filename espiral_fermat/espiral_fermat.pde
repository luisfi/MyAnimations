/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
* Autor: Luis Alberto Oropeza Vilchis <luis.oropeza.129@gmail.com>
*/

/*
* Animación que muestra la generación de la Espiral de Fermat.
*/

import java.util.*; // Para utilizar la clase Vector

FermatSpiral spiral;

void setup() {
  size(512, 512);
  spiral = new FermatSpiral(12*PI, 40, 0.6);
}

void draw() {
  background(255);
  strokeWeight(4);
  stroke(150, 0, 0);
  // spiral.drawLines(); // Para dibujar de forma continua
  spiral.drawPoints(); // Para dibujar con puntos
}


public class Point {
  float x, y;

  Point() {
    this.x = 0.0;
    this.y = 0.0;
  }

  Point(float x, float y) {
    this.x = x;
    this.y = y;
  }

  public void setX(float x) {
    this.x = x;
  }

  public float getX() {
    return x;
  }

  public void setY(float y) {
    this.x = y;
  }

  public float getY() {
    return y;
  }

  public void translateX(float offset) {
    this.x += offset;
  }
}


class PolarCoordinate {
  float r, theta;
  float x, y;

  PolarCoordinate(float r, float theta) {
   this.r = r;
   this.theta = theta;
   this.x = calculateX();
   this.y = calculateY();
  }

  public float getRadius() {
    return r;
  }

  public float getTheta() {
    return theta;
  }

  public float getX() {
    return x;
  }

  public float getY() {
    return y;
  }

  public void setRadius(float r) {
    this.r = r;
  }

  public void setTheta(float t) {
    this.theta = t;
  }

  public void setX(float x) {
    this.x = x;
  }

  public void setY(float y) {
    this.y = y;
  }

  private float calculateX() {
    return r*cos(theta);
  }

  private float calculateY() {
    return r*sin(theta);
  }
}


class FermatSpiral {

  float period;
  float scale;
  float precision;
  Vector<PolarCoordinate> positiveCoordinates;
  Vector<PolarCoordinate> negativeCoordinates;
  PShape positiveCurve;
  PShape negativeCurve;
  int index;
  boolean reverse;

  FermatSpiral(float period, float scale, float precision) {
    this.period = period;
    this.scale = scale;
    this.precision = precision;
    this.positiveCoordinates = new Vector(10);
    this.negativeCoordinates = new Vector(10);
    this.index = 0;
    calculateCoordinates();
    reverse = false;
  }

  // Dibuja la espiral con puntos
  private void drawPoints() {
    if (!reverse) {
      for (int i=0; i < index; i++){
        point(positiveCoordinates.elementAt(i).getX()*scale + width/2,
                    positiveCoordinates.elementAt(i).getY()*scale + height/2);
      }
      for (int j=0; j < index; j++) {
        point(negativeCoordinates.elementAt(j).getX()*scale + width/2,
                    negativeCoordinates.elementAt(j).getY()*scale + height/2);
      }

      if (++index >= negativeCoordinates.size()) reverse = true;
    } else {
      if (--index <= 0) reverse = false;
      for (int i=index; i >= 0; i--) {
        point(positiveCoordinates.elementAt(i).getX()*scale + width/2,
          positiveCoordinates.elementAt(i).getY()*scale + height/2);
      }
      for (int i=index; i >= 0; i--) {
        point(negativeCoordinates.elementAt(i).getX()*scale + width/2,
          negativeCoordinates.elementAt(i).getY()*scale + height/2);
      }
    }
  }

  // Dibuja la espiral a través de curvas de Bezier
  public void drawLines() {
    if (!reverse) {
      if (++index >= negativeCoordinates.size())  {
        reverse = true;
        delay(2000);
      }
      stroke(70, 204, 35);
      beginShape();
        noFill();
        for (int i=0; i < index; i++){
          curveVertex(positiveCoordinates.elementAt(i).getX()*scale + width/2,
                      positiveCoordinates.elementAt(i).getY()*scale + height/2);
        }
      endShape();

      stroke(255, 32, 18);
      beginShape();
        noFill();
        for (int j=0; j < index; j++) {
          curveVertex(negativeCoordinates.elementAt(j).getX()*scale + width/2,
                      negativeCoordinates.elementAt(j).getY()*scale + height/2);
        }
      endShape();


    } else {
      if (--index <= 0) {
        reverse = false;
        delay(2000);
      }
        stroke(70, 204, 35);
        beginShape();
          noFill();
          for (int i=index; i >= 0; i--)
              curveVertex(positiveCoordinates.elementAt(i).getX()*scale + width/2,
                          positiveCoordinates.elementAt(i).getY()*scale + height/2);
        endShape();

        stroke(255, 32, 18);
        beginShape();
          noFill();
          for (int i=index; i >= 0; i--)
              curveVertex(negativeCoordinates.elementAt(i).getX()*scale + width/2,
                          negativeCoordinates.elementAt(i).getY()*scale + height/2);
        endShape();
    }
  }

  private void calculateCoordinates() {
    for (float i=0; i <= period; i += precision) {
      if (0==i) {
        // Se repiten los primeros puntos, ya que son la referencia
        positiveCoordinates.add(new PolarCoordinate(sqrt(i), i));
        negativeCoordinates.add(new PolarCoordinate(-sqrt(i), i));
      }
      positiveCoordinates.add(new PolarCoordinate(sqrt(i), i));
      negativeCoordinates.add(new PolarCoordinate(-sqrt(i), i));
    }
  }
}
