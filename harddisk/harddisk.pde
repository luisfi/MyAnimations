/**
 * Animación sobre el funcionamiento del disco duro para la asignatura de
 * Dispostivios de almacenamiento y de E/S, Facultad de Ingeniería  de la
 * UNAM, plan 2010.
 * Equipo:
 *  --> Barriga Martinez Diego Alberto
 *  --> Bustos Ramírez Luis Enrique
 *  --> Oropeza Vilchis Luis Alberto
 *  --> Rueda Rivera Raul Adrian
 *
 * Funcionamiento:
 *  Mouse:
 *      Mouse Derecho: flujo magnético hacia la derecha
 *      Mouse Izquierdo: flujo magnético hacia la izquierda
 *      Mouse Intermedio: desactiva flujo
 * Teclado:
 *      u o U: aumentar velocidad del disco
 *      d o D: decrementar velocidad el disco
 *      n o N: desactivar flujo magnético
 * ToDo:
 *  Dibujar líneas de flujo en el cabezal de escritura
 *  Clase para manejar la paleta de colores
 *  Dibujar flecha a partir del centro de la línea
 */


import java.util.*; // To use ArrayList and Iterator classes
import java.lang.Math.*; // To use Math.random static method

int NUM_BITS = 15; // Number of bits on screen
BitGenerator generator;
DiskHead dh;

void setup() {
    // frameRate(10); // Change frame rate
    size(800, 600);
    dh = new DiskHead(width, height);
    generator = new BitGenerator(width/NUM_BITS, 8*height/40, width, 26*height/40);
}

void draw() {
    background(0, 0, 0); // Redraw background
    dh.draw();
    generator.draw();
    drawBottomLayer();
    dh.magnetizeBits(generator.getBits());
}


void drawBottomLayer() {
    rectMode(CENTER);
    fill(90);
    stroke(90);
    strokeWeight(2);
    rect(width/2, 7*height/8, width, height/4);
    fill(255);
    textSize(30);
    text("Capa inferior", (0.80)*width/2, 7*height/8);
}

void mousePressed() {
    if (mouseButton == LEFT)
        dh.setActive(DiskHead.LEFT);
    else if (mouseButton == RIGHT)
        dh.setActive(DiskHead.RIGHT);
    else
        dh.setInactive();
}


void mouseReleased() {
    // Do something
}

void keyPressed() {
    switch(key) {
        case 'n':
        case 'N':
            dh.setInactive();
            break;
        case 'u':
        case 'U':
            generator.setVelocity(generator.getVelocity() + 1);
            break;
        case 'd':
        case 'D':
            generator.setVelocity(generator.getVelocity() - 1);
            break;
        default:
            println("Not used: " + key);
    }
}

class BitGenerator {
    private ArrayList<Bit> bits;
    private int counter; // To create new rectangle
    private int w, h;
    private int velocity;
    private int xInitial, yInitial;

    BitGenerator(int figureWidth, int figureHeigth, int xInitial, int yInitial) {
        this.w = figureWidth;
        this.h = figureHeigth;
        this.xInitial = xInitial;
        this.yInitial = yInitial;
        this.counter = 0;
        this.velocity = 1;
        this.bits = new ArrayList<Bit>(2);
        this.bits.add(new Bit(xInitial + w, yInitial, w, h));
    }

    void draw() {
        cleanBits();
        generate();
        for (Bit b: bits) {
            b.move(velocity);
            b.draw();
        }
    }

    private void generate() {
        if (counter >= w) {
            bits.add(new Bit(xInitial + w, yInitial, w, h));
            counter = velocity; // Offset in draw
            } else {
                counter += velocity; // Last bit steps
            }
        }

    private void cleanBits() {
        for (final Iterator b = bits.iterator(); b.hasNext(); ) {
            Bit t = (Bit) b.next();
            if (t.getX() < -w/2) { // uses w/2 because it uses center position
                b.remove(); // if is out of screen it's removed
            }
        }
    }

    void setVelocity(int velocity) {
        if (velocity >= 1 && velocity <= 5) {
            this.velocity = velocity;
        }
    }

    int getVelocity() {
      return velocity;
    }

    ArrayList getBits() {
        return bits;
    }
}

class Bit {
    private final color RED = color(255, 0, 0);
    private final color BLUE = color(0, 0, 255);
    private final color GRAY = color(128);
    private int x, y;
    private int w, h;
    private color statusColor;
    private ArrayList<Line> magneticFlux;
    private float lineLarge; // Magnetic flux lines size

    Bit(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.neutral();
        this.initFullFixedMagneticFlux(); // Create magnetic flux lines
    }

    // Magnetic flux lines position are random
    private void initRandomMagneticFlux() {
        this.magneticFlux = new ArrayList<Line>(5);
        this.lineLarge = w/5;
        float xPosLimit = -w/2 + 1.2*lineLarge;
        float xNegLimit = w/2 - 1.2*lineLarge;
        float yPosLimit = h/2 - lineLarge;
        float yNegLimit = -h/2 + lineLarge;
        for (int i=0; i < 30; i++) {
            this.magneticFlux.add(new Line(x, y,
                 (float)(xNegLimit + Math.random() * (xPosLimit - xNegLimit)),
                 (float)(yNegLimit + Math.random() * (yPosLimit - yNegLimit)),
                 lineLarge));
        }
        for (Line l: magneticFlux) {
            l.randomRotate();
        }
    }

    // 'Y' positions of magnetic flux lines are random, X positions are fixed
    private void initRandomYMagneticFlux() {
        this.magneticFlux = new ArrayList<Line>(5);
        this.lineLarge = w/5;
        int xPosLimit = (int) (w/2 - 1.2*lineLarge);
        int xNegLimit = (int) (-w/2 + 1.2*lineLarge);
        int yPosLimit = (int) (h/2 - lineLarge);
        int yNegLimit = (int) (-h/2 + lineLarge);
        for (int i = yNegLimit; i < yPosLimit; i+=(int)lineLarge) {
            for (int j=0; j<4; j++) {
                this.magneticFlux.add(new Line(x, y,
                    (float)(xNegLimit + Math.random() * (xPosLimit - xNegLimit)),
                    i, lineLarge));
            }
        }
        for (Line l: magneticFlux) {
            l.randomRotate();
        }
    }

    // Magnetic flux lines postions are fixed
    private void initFullFixedMagneticFlux() {
        this.magneticFlux = new ArrayList<Line>(5);
        this.lineLarge = w/5;
        int xPosLimit = (int) (w/2 - 1.2*lineLarge);
        int xNegLimit = (int) (-w/2 + 1.2*lineLarge);
        int yPosLimit = (int) (h/2 - lineLarge);
        int yNegLimit = (int) (-h/2 + lineLarge);
        for (int i = yNegLimit; i < yPosLimit; i+=(int)lineLarge) {
            for (int j=xNegLimit; j<xPosLimit; j+=(int)lineLarge) {
                this.magneticFlux.add(new Line(x, y,
                    j, i, lineLarge));
            }
        }
        for (Line l: magneticFlux) {
            l.randomRotate();
        }
    }

    // 'X' positions of magnetic flux lines are random, Y positions are fixed
    private void initRandomXMagneticFlux() {
        this.magneticFlux = new ArrayList<Line>(5);
        this.lineLarge = w/5;
        int xPosLimit = (int) (w/2 - 1.2*lineLarge);
        int xNegLimit = (int) (-w/2 + 1.2*lineLarge);
        int yPosLimit = (int) (h/2 - lineLarge);
        int yNegLimit = (int) (-h/2 + lineLarge);
        for (int i = xNegLimit; i < xPosLimit; i+=(int)lineLarge) {
            for (int j=0; j<15; j++) {
                this.magneticFlux.add(new Line(x, y, i,
                    (float)(yNegLimit + Math.random() * (yPosLimit - yNegLimit)),
                    lineLarge));
            }
        }
        for (Line l: magneticFlux) {
            l.randomRotate();
        }
    }

    // Only 6 fixed magnetic flux lines are created
    private void initMagneticFlux() {
        this.magneticFlux = new ArrayList<Line>(5);
        this.lineLarge = w/5;
        this.magneticFlux.add(new Line(x, y, -w/2+(1.2*lineLarge), lineLarge/2, lineLarge));
        this.magneticFlux.add(new Line(x, y, w/2-(1.2*lineLarge), lineLarge/2, lineLarge));
        this.magneticFlux.add(new Line(x, y, -w/2+(1.2*lineLarge), -h/2+(1.2*lineLarge), lineLarge));
        this.magneticFlux.add(new Line(x, y, w/2-(1.2*lineLarge), -h/2+(1.2*lineLarge), lineLarge));
        this.magneticFlux.add(new Line(x, y, -w/2+(1.2*lineLarge), h/2-(1.2*lineLarge), lineLarge));
        this.magneticFlux.add(new Line(x, y, w/2-(1.2*lineLarge), h/2-(1.2*lineLarge), lineLarge));
        for (Line l: magneticFlux) {
            l.randomRotate();
        }
    }

    void move(int velocity) {
        x -= velocity;
    }

    void draw() {
        rectMode(CENTER);
        stroke(255);
        strokeWeight(2);
        fill(statusColor);
        rect(x, y, w, h);
        // fill(255,255,255);
        // textSize(10);
        // text(x ,x, y); // To debug position
        for (Line l: magneticFlux) {
            l.draw();
            l.updatePosition(x, y);
        }
    }

    void positive() {
        this.statusColor = BLUE;
        for (Line l: magneticFlux) {
            l.leftHead();
        }
    }

    void negative() {
        this.statusColor = RED;
        for (Line l: magneticFlux) {
            l.rightHead();
        }
    }

    void neutral() {
        this.statusColor = GRAY;
    }

    void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    int getX() {
        return x;
    }

    int getY() {
        return y;
    }
}


class DiskHead {
    private final color INACTIVE_WIRE = color(184, 115, 51);
    private final color ACTIVE_WIRE = color(244, 244, 66);
    public final static int RIGHT = 0;
    public final static int LEFT = 1;
    public final static int INACTIVE = 2;
    private int w, h;
    private int center;
    private boolean active;
    private color wireColor;
    private int direction;

    DiskHead(int width, int height) {
        this.w = width;
        this.h = height;
        this.center = width/2 + width/8 - 9*height/80; // + w/8 - 9*h/80 = offset on figure
        this.active = false;
        this.wireColor = INACTIVE_WIRE;
        this.direction = INACTIVE;
    }

    void draw() {
        noStroke();
        fill(200);
        rectMode(CENTER);
        rect(w/2, 10*h/40, w/4, 4*h/40);
        rectMode(CORNER);
        rect(w/2 - w/8, 12*h/40, 4*h/40, 19*h/80);
        rect(w/2 + w/8 - 4*h/40, 12*h/40, 4*h/40, 19*h/80);
        rect(w/2 - w/8 + 4*h/40, 18*h/40, 4*h/40, 7*h/80);
        stroke(wireColor);
        strokeWeight(3);
        point(w/2 + w/8 - 9*h/80, 18*h/40);
        for (int i = 0, j = 10; i < 10; i ++)
            line(w/2 + w/8 - 4*h/40 - 5, h/3 + i*j, w/2 + w/8 + 5, h/3 + i*j);
    }

    void setActive(int direction) {
        this.active = true;
        this.direction = direction;
        this.wireColor = ACTIVE_WIRE;
    }

    void setInactive() {
        this.active = false;
        this.wireColor = INACTIVE_WIRE;
    }

    void magnetizeBits(ArrayList<Bit> bits) {
        if (active) {
            for (Bit b: bits) {
                if ((b.getX() > center - 3) && (b.getX() < center + 3)) {
                    if (direction == LEFT)
                        b.positive();
                    else if (direction == RIGHT)
                        b.negative();
                    break; // only once bit in header
                }
            }
        }
    }
}

// Class to draw lines with arrow in both sides
class Line {
    private PVector vector;
    private float xi, yi;
    private float angle;
    private float large;
    private float xOffset, yOffset;
    Line(float x0, float y0, float large) {
        this.xi = x0;
        this.yi = y0;
        this.large = large;
        this.vector = new PVector(0, large);
        this.xOffset = 0;
        this.yOffset = 0;
    }

    Line(float x0, float y0, float xOffset, float yOffset, float large) {
        this.xi = x0;
        this.yi = y0;
        this.large = large;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.vector = new PVector(0, -large);
    }

    public void draw() {
        arrowLine(xi + xOffset, yi + yOffset,
             xi + xOffset + vector.x, yi + yOffset + vector.y, 0, 45, false);
    }

    private void arrowLine(float x0, float y0, float x1, float y1,
         float startAngle, float endAngle, boolean solid) {
        strokeWeight(2);
        fill(0, 255, 0);
        stroke(0, 255, 0);
        line(x0, y0, x1, y1);
        if (startAngle != 0) {
            arrowhead(x0, y0, atan2(y1 - y0, x1 - x0), startAngle, solid);
        }
        if (endAngle != 0) {
            arrowhead(x1, y1, atan2(y0 - y1, x0 - x1), endAngle, solid);
        }
    }

    private void arrowhead(float x0, float y0, float lineAngle,
                      float arrowAngle, boolean solid) {
        float phi;
        float x2;
        float y2;
        float x3;
        float y3;
        final float SIZE = 4;

        x2 = x0 + SIZE * cos(lineAngle + arrowAngle);
        y2 = y0 + SIZE * sin(lineAngle + arrowAngle);
        x3 = x0 + SIZE * cos(lineAngle - arrowAngle);
        y3 = y0 + SIZE * sin(lineAngle - arrowAngle);
        if (solid) {
            triangle(x0, y0, x2, y2, x3, y3);
        } else {
            line(x0, y0, x2, y2);
            line(x0, y0, x3, y3);
        }
    }

    public void updatePosition(float x, float y) {
        this.xi = x;
        this.yi = y;
    }

    public void randomRotate() {
        this.angle = (float)Math.random() * 2*PI;
        this.vector.rotate(angle);
    }

    public void topHead() {
        this.vector.rotate(-angle);
        this.angle = 0;
    }

    public void bottomHead() {
        this.vector.rotate(-angle);
        this.angle = PI;
        this.vector.rotate(angle);
    }

    public void leftHead() {
        this.vector.rotate(-angle);
        this.angle = PI/2;
        this.vector.rotate(angle);
    }

    public void rightHead() {
        this.vector.rotate(-angle);
        this.angle = -PI/2;
        this.vector.rotate(angle);
    }
}
