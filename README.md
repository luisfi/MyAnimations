Animaciones
=========================

Pequeñas animaciones realizadas como hobby con la herramienta
[Processing](https://processing.org/).

* [Disco Duro](harddisk/harddisk.pde)

  Intenta ilustrar de forma muy rudimentaria el funcionamiento del mecanismo del
  disco duro, al escribir 1's o 0's magnetizando las celdas, al inducir una
  corriente, en uno u otro sentido, por un cable.

  ToDo:

    * Recibir cadenas (bits, bytes o palabras) y escribir en las celdas

* [Funciones senoidales](senoidal/senoidal.pde)

  Animación para mostrar señales senoidales generandose en el tiempo.

  ToDo:

  * Operaciones con las funciones (eg. sumar dos funciones y dibujar el resultado).

* [Espiral](espiral_fermat/espiral_fermat.pde)

  Se crea la [espiral de fermat](https://es.wikipedia.org/wiki/Espiral_de_Fermat);
  se puede modificar fácilmente para dibujar de forma continua (curvas de
  Bezier) o discreta (puntos).

  ToDo:

  * Centro de la figura como parámetro
  * Colores como parámetros (pueden ser dos colores)
  * Ajustar velocidad de animación

## Licencia

[GPL3](COPYING)
